#!/bin/bash

# ==============================================================================

function printhelp {
echo '
NAME

  makemovie - convert a batch of images to a movie

USAGE

  $ makemovie FMT OUTPUT

DESCRIPTION

  Convert a batch of images to a movie.

EXAMPLES

  $ makemovie "results_%03d.png" results

    Convert a batch of files "results_000.png", "results_001.png", ... to a
    movie "results.mp4".

  $ for f in results*.png; do n=${f%.*}; convert -flatten $n.png $n\_white.png; done
  $ makemovie "results_%03d_white.png" results

    Convert a batch of files "results_000.png", "results_001.png", ... first to
    PNGs with a white background and then to a movie "results.mp4"

COMPRESSION

  This function create a movie without any compression. To apply compression
  different programs can be used. Suggestions:

  - handbrake (OSx).

TIPS

  ffmpeg -r 2 -i $1 $2.mp4

COPYRIGHT

  T.W.J. de Geus
  tom@geus.me
  www.geus.me
'
}

# ==============================================================================

# check for options
for i in "$@"
do
  case $i in
    -h|--help)
    printhelp; exit 0
    shift
    ;;
    *)
    # unknown option
    ;;
  esac
done

# main program
if [ "$(uname)" == "Darwin" ]; then
  # Do something under Mac OS X platform
  ffmpeg -vsync 1 -f image2 -i $1 -vcodec copy $2.mov
elif [ "$(expr substr $(uname -s) 1 5)" == "Linux" ]; then
  # Do something under Linux platform
  ffmpeg -i $1 $2.mp4
elif [ "$(expr substr $(uname -s) 1 10)" == "MINGW32_NT" ]; then
  # Do something under Windows NT platform
  echo "windows"
fi
