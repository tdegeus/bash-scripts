#!/bin/bash

# ==============================================================================

function printhelp {
echo "\

NAME

  filemod - recursively modify file-names

USAGE

  $ filemod FIND REPLACE PATH [OPTIONS]

DESCRIPTION

  Recursively modify file-names. The files are selected using the 'find'
  command, as follows:

    $ find PATH [OPTIONS]

  The FIND string is replaced by the REPLACE string in all the found
  files/folders.

EXAMPLE

  $ filemod 'temp_' '' .

    Removes the word 'temp_' from all files/folders in the current directory
    and all of its subdirectories.

  $ filemod ' ' '_' . -mindepth 1 -maxdepth 1

    Replaces all spaces by underscores in the files/folders in the current
    directory.

OPTIONS

  -h, --help
    Print help.

  -v, --version
    Print version information.

SEE ALSO

  $ man find

COPYRIGHT

  T.W.J. de Geus
  tom@geus.me
  www.geus.me
"
}

# ------------------------------------------------------------------------------

function printversion {
echo "\

Version 1.0, August 2013
"
}

# ------------------------------------------------------------------------------

function printchangelog {
echo "\

Version 1.0, August 2013
  - created first version [TdG]

Known Bugs
  none at this point [TdG]

Possible extensions
  none at this point [TdG]

Editors
  T.W.J. de Geus [TdG]
"
}

# ------------------------------------------------------------------------------

function printerror () {
echo $1
echo "Type '"$0" --help' for more information"
exit 1
}

# ==============================================================================

# print help/version
# ------------------

# check the bar minimum number of arguments
if [ $# -lt 1 ]; then
  printerror "Unknown number of input arguments"
fi
# optional print help of version
if [ "$1" == "-h" ] || [ "$1" == "--help" ]; then
  printhelp; exit 0
elif [ "$1" == "-v" ]; then
  printversion; exit 0
elif [ "$1" == "--version" ]; then
  printchangelog; exit 0
fi

# read arguments and options
# --------------------------

# check the minimum number of arguments
if [ $# -lt 3 ]; then
  printerror "Unknown number of input arguments"
fi

# find/replace string: first two arguments; remaining arguments
# argument for find command
fnd="$1"; shift
rpl="$1"; shift
fol="$1"; shift
opt=("$@")

# convert options: read/exclude mindepth/maxdepth commands
# --------------------------------------------------------

# set defaults
rmin=0
rmax=0

# set new options list
tmp=()
itmp=1

# loop over options
for (( iopt=0 ; iopt<${#opt[@]} ; iopt++ )); do

  # detect mindepth/maxdepth arguments, and store list index and value
  if [ "${opt[$iopt]}" == "-mindepth" ]; then
    rmin=1
    imin=$iopt
    dmin="${opt[$(($iopt+1))]}"
  elif [ "${opt[$iopt]}" == "-maxdepth" ]; then
    rmax=1
    imax=$iopt
    dmax="${opt[$(($iopt+1))]}"
  fi

  # set default: include option
  wrt=1
  # detect mindepth/maxdepth options and exclude if found
  if [ $rmin -eq 1 ]; then
    if [ $iopt -eq $imin ] || [ $iopt -eq $(($imin+1)) ]; then
      wrt=0
    fi
  fi
  if [ $rmax -eq 1 ]; then
    if [ $iopt -eq $imax ] || [ $iopt -eq $(($imax+1)) ]; then
      wrt=0
    fi
  fi
  # include option in new option list
  if [ $wrt -eq 1 ]; then
    tmp[$itmp]="${opt[$iopt]}"
    ((itmp++))
  fi
done


# ==============================================================================

# change separator
IFSBAK=$IFS
IFS=$'\n'

# initiate current depth
if [ $rmin -eq 1 ]; then
  idepth=$dmin
else
  idepth=0
fi

# initiate condition
con=1
# loop over depths: defined by maxdepth and/or empty search (below)
while [ $con -eq 1 ]; do

  # find files/folders
  f=(`find $fol -mindepth $idepth -maxdepth $idepth ${tmp[@]}`)
  # loop for replace
  for (( i=0; i<${#f[@]}; i++ )); do
    # set old name
    o=${f[$i]}
    # set new new
    n=`echo $o | sed "s/$fnd/$rpl/g"`
    # replace if old/new name are different
    if [ "$o" != "$n" ]; then
      mv "$o" "$n"
    fi
  done

  # break if maximum depth has be reached
  if [ $rmax -eq 1 ]; then
    if [ $idepth -eq $dmax ]; then con=0; fi
  fi
  # break if find command is empty
  if [[ -z `find . -mindepth $idepth -maxdepth $idepth` ]]; then con=0; fi

  # proceed to next depth
  ((idepth++))

done

# change back file separator
IFS=$IFSBAK

# close the program
exit 0
